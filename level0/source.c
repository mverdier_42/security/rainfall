int		main(int ac, char **av)
{
	if (atoi(av[1]) == 423)		// 0x1a7 = 423
	{
		char *shell = strdup("/bin/sh");
		int gid = getgid();
		int uid = getuid();
		setresgid(gid, gid, gid);
		setresuid(uid, uid, uid);
		execv(shell, &shell);
	}
	else
		write(1, "No !\n", 5);
	return (0);
}
