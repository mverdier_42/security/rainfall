#include <stdio.h>
#include <string.h>

char	*p()
{
	int		ret;
	char	*str;
	char	buf[64];

	fflush(stdin);
	gets(&buf);
	ret = buf + 80;
	if ((ret & 0xb0000000) == 0xb0000000) {
		printf("(%p)\n", ret);
		exit(1);
	}
	else {
		puts(buf);
		str = strdup(buf);
	}
	return (str);
}

void	main()
{
	p();
}
