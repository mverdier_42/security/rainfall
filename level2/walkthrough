Nous avons ici un binaire level2 qui attend un input et l'affiche ensuite.
En regardant le binaire avec gdb, celui-ci appel une fonction p, qui gets et
affiche ensuite la valeur entrée.

Un simple buffer overflow comme l'exercice d'avant, avec un shellcode, devrait
suffir, mais la fonction p protège les adresses dans la range 0xb*******.

On va d'abbord écrire un shellcode en assembleur, qui va utiliser la fonction
execve pour executer un cat sur /home/user/level3/.pass, ensuite on va compiler
le code assembleur avec `nasm -f elf32 shellcode.asm` puis on va utiliser la
commande 'objdump -d' sur le .o pour récupérer le code binaire du programme asm.

On va ensuite mettre ce shellcode dans une variable d'environnement :
`export SHELLCODE=$(perl shellcode.pl)`

Quand on va lancer le binaire level2, les variables d'environnement seront
stockées dans le programme, on va donc vouloir récupérer l'adresse de notre
variable d'environnement pour pouvoir executer son shellcode.

Pour ça on a fait un petit programme C qui utilise la fonction 'getenv()', et
calcule la différence de taille du nom (+ le path) du programme et du level2 * 2
pour avoir la bonne adresse de notre variable dans le programme level2.
(Le nom et le path du programme sont contenus deux fois dans le programme).

Reste ensuite à injecter cette adresse dans le programme level2. On voudrait
simplement réecrire sur l'adresse de retour de la fonction, mais cette valeur
est testée juste après pour vérifier qu'elle n'est pas dans la range 0xb*******.
On ne peut donc pas mettre l'adresse de notre variable à la place car elle se
trouve justement dans cette range.

Au moment d'executer l'instruction ret, la stack a été vidée par l'instruction
leave. L'instruction ret pop la valeur au top de la stack dans eip, puis le
programme continue son execution à partir de eip. Si on réecrit l'adresse de
l'instruction ret à la place de l'adresse de retour de la fonction, alors
au moment de ret le programme va executer une deuxième fois ret, donc pop la
valeur au top de la stack dans eip, puis continuer son execution à partir de eip.

La solution ici est donc au moment d'injecter notre shellcode de faire en sorte
de réecrire l'adresse de l'instruction ret à la place de l'adresse de retour de
la fonction, et de coller l'adresse de notre variable juste derrière, de sorte
qu'a la deuxième execution de ret, l'adresse de notre shellcode soit placée dans
eip, et ainsi qu'il soit executé.

L'adresse de retour de la fonction se trouve à 80 octets du début du buffer, on
va donc envoyer 80 caractères 'A', puis l'adresse de ret (0x0804853e), puis
l'adresse de notre variable (0xbffff8c2), et nous récupèrerons le mot de passe
du level3 !
