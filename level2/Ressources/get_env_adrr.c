#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// gcc -m32 for 32 bits target
// gcc -m64 for 64 bits target
int		main(int ac, char **av)
{
	char	*ptr;

	if (ac < 3) {
		printf("Usage: %s <env var name> <target program>\n", av[0]);
		return (0);
	}
	ptr = getenv(av[1]);
	ptr += (strlen(av[0]) - strlen(av[2])) * 2;
	printf("%s address : %p\n", av[1], ptr);
	return (0);
}
