#include <stdio>
#include <string.h>
#include <stdlib.h>

// class N = 108 bytes
class N {
	public:
		N(int n);
		void	*setAnnotation(char *str);

	private:
		char	_str[100];
		int		_nb;
}

N::N(int n) {
	this->_nb = n;
}

void	*N::setAnnotation(char *s) {
	return (memcpy(this->_str, s, strlen(s)));
}

int		main(int ac, char **av)
{
	N		*n1;
	N		*n2;

	if (ac < 2)
		exit(1);

	n1 = new N(5);
	n2 = new N(6);
	n1->setAnnotation(av[1]);
	(**n2)(n2, n1);
}
