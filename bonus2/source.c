#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int		language = 0;

void	greetuser(char *buffer)
{
	char	str[72];

	if (language == 0)
		memcpy(str, "Hello ", 7);
	if (language == 1)
		memcpy(str, "Hyvää päivää", 19);
	if (language == 2)
		memcpy(str, "Goedemiddag! ", 13);
	strcat(str, buffer);
	puts(str);
}

int		main(int ac, char **av)
{
	char		esp[80];
	char		buffer[76];
	char		*env;

	if (ac != 3)
		return (1);

	memset(buffer, 0, 76); // len = 19 * 4 (dword)
	strncpy(buffer, av[1], 40);
	strncpy(buffer + 40, av[2], 32);
	env = getenv("LANG");
	if (env != NULL) {
		if (memcmp(env, "fi", 2) == NULL)
			language = 1;
		else if (memcmp(env, "nl", 2) == NULL)
			language = 2;
	}
	strncpy(esp, buffer, 76);
	greetuser(esp);
	return (0);
}
