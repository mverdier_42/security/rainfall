#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

char	*p(char *str)
{
	char	buffer[4096];
	char	*tmp;

	puts(" - ");
	read(0, buffer, 4096);
	tmp = strchr(buffer, '\n');
	*tmp = '\0';
	return (strncpy(str, buffer, 20));
}

char	*pp(char *str)
{
	char	s1[20];
	char	s2[20];

	p(s1);
	p(s2);
	strcpy(str, s1);
	*(int16_t*)(str + strlen(str)) = " \0"; // "\0" = " \0"
	return (strcat(str, s2));
}

int		main()
{
	char	buffer[50];

	pp(buffer);
	puts(buffer);
	return (0);
}
