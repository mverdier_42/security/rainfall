#include <stdio.h>
#include <unistd.h>
#include <string.h>

int		main(int ac, char **av)
{
	FILE	*passwd;
	char	buffer[132];

	passwd = fopen("/home/user/end/.pass", "r");
	memset(buffer, 0, 132);
	if (passwd == NULL || ac != 2)
		return (-1);
	fread(buffer, 1, 66, passwd);
	*(buffer + atoi(av[1])) = '\0';
	fread(buffer + 66, 1, 65, passwd);
	fclose(passwd);
	if (strcmp(buffer, av[1]) == 0)
		execl("/bin/sh", "sh");
	else
		puts(buffer + 66);
}
