#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int		main()
{
	char	*auth;
	char	*service;
	char	*str;		// &var_88

	while (1) {
		printf("%p, %p \n", auth, service);
		if (fgets(str, 128, stdin) == 0)
			break ;
		if (strcmp(str, "auth ") == 0) {
			auth = malloc(4);
			*auth = '\0';
			if (strlen(str + 5) < 32)
				strcpy(auth, str + 5);
		}
		if (strcmp(str, "reset") == 0)
			free(auth);
		if (strcmp(str, "service") == 0)
			service = strdup(str + 7);
		if (strcmp(str, "login") != 0)
			continue ;
		if (*(auth + 32) != '\0')
			system("/bin/sh");
		else
			fwrite("Password:\n", 1, 10, stdout);
	}
	return (0);
}
