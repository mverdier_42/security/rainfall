#include <stdio.h>
#include <string.h>

void	o()						// Target
{
	system("/bin/sh");
	exit(1);
}

int		n()
{
	char	buffer[512];

	fgets(buffer, 512, stdin);
	printf(buffer);				// Vulnerability
	return(exit(1));
}

void	main()
{
	n();
}
